
#------------------------------------------------------------------
#  Terraform
#
# Generate, Store and Retrieve Password Using AWS Secrets Manager
#
#------------------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}

resource "aws_db_instance" "prod" {
    identifier             = "prod-mysql-rds"
    allocated_storage      = 20
    storage_type           = "gp2"
    engine                 = "mysql"
    engine_version         = "5.7"
    instance_class         = "db.t2.micro"
    # parameter_group_name   = "default.mysql.5.7"
    skip_final_snapshot    = true
    apply_immediately      = true
    username               = "administrator"
 #   password               = "0aqtj2If8bdwVdhAgOGv"
    # password               = random_password.main.result
    password               = data.aws_secretsmanager_secret_version.rds_password.secret_string
}


# Generate password
resource "random_password" "main" {
  length           = 20
#   min_lower        = 5
#   min_numeric      = 5
#   min_upper        = 5
#   min_special      = 5
  special          = true  # By default !@#$%&*()-_=+[]{}<>:?
  override_special = "#!()_-"
}

# # Store password
resource "aws_secretsmanager_secret" "rds_password" {
  name                    = "/prod/rds/password"
  description             = "Password for my RDS Databases"
  recovery_window_in_days = 0
}

resource "aws_secretsmanager_secret_version" "rds_password" {
  secret_id     = aws_secretsmanager_secret.rds_password.id
  secret_string = random_password.main.result
}

# Retrive password
data "aws_secretsmanager_secret_version" "rds_password" {
    secret_id = aws_secretsmanager_secret.rds_password.id
    depends_on = [
      aws_secretsmanager_secret_version.rds_password
    ]
}

####################################
output "rds_endpoint" {
  value = aws_db_instance.prod.endpoint
}

output "rds_address" {
  value = aws_db_instance.prod.address
}

output "rds_port" {
  value = aws_db_instance.prod.port
}

output "rds_username" {
  value = aws_db_instance.prod.username
}