#-------------------------------------------------------------------------------
#  Terraform
#
# Provision VPCs using Modules
#
#-------------------------------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}

terraform {
  backend "s3" {
    bucket = "terraform-s3-vbyte-bucket"         // Bucket where to SAVE Terraform State
    key    = "module/ProjectA/terraform.tfstate" // Object name in the bucket to SAVE Terraform State
    region = "eu-central-1"                      // Region where bucket created
  }
}

module "my_vpc_default" {
  source = "../modules/network"
}

module "my_vpc_staging" {
  source               = "../modules/network"
  env                  = "staging"
  vpc_cidr             = "10.100.0.0/16"
  public_subnet_cidrs  = ["10.100.1.0/24", "10.100.2.0/24", "10.100.3.0/24"]
  private_subnet_cidrs = []
}

module "my_vpc_prod" {
  source               = "../modules/network"
  env                  = "prod"
  vpc_cidr             = "10.200.0.0/16"
  public_subnet_cidrs  = ["10.200.1.0/24", "10.200.2.0/24", "10.200.3.0/24"]
  private_subnet_cidrs = ["10.200.11.0/24", "10.200.12.0/24", "10.200.13.0/24"]
  tags = {
    Project = "Terraform-Prod"
  }
}
