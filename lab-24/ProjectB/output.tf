output "server_standalone_ip" {
  value = module.server_standalone.web_server_public_ip
}

output "ec2servers_loop_count_ips" {
  value = module.ec2servers_loop_count[*].web_server_public_ip
}

output "servers_loop_foreach" {
  value = values(module.servers_loop_foreach)[*].web_server_public_ip
}