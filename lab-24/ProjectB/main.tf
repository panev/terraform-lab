#-------------------------------------------------------------------------------
#  Terraform
#
# Provision VPCs using Modules
#  # source  = "git@gitlab.com:panev/terraform-lab.git//lab-24/modules/ec2?ref=yourbranchname" # <-- your branch
#
#-------------------------------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}

terraform {
  backend "s3" {
    bucket = "terraform-s3-vbyte-bucket"         // Bucket where to SAVE Terraform State
    key    = "module/ProjectB/terraform.tfstate" // Object name in the bucket to SAVE Terraform State
    region = "eu-central-1"                      // Region where bucket created
  }
}


module "vpc_prod" {
  # source               = "../modules/network"
  source               = "git@gitlab.com:panev/terraform-lab.git//lab-24/modules/network"
  env                  = "prod"
  vpc_cidr             = "10.200.0.0/16"
  public_subnet_cidrs  = ["10.200.1.0/24", "10.200.2.0/24", "10.200.3.0/24"]
  private_subnet_cidrs = ["10.200.11.0/24", "10.200.12.0/24", "10.200.13.0/24"]
  tags = {
    Project = "Terraform-Prod"
  }
}

module "server_standalone" {
  # source    = "../modules/ec2"
  source               = "git@gitlab.com:panev/terraform-lab.git//lab-24/modules/ec2"
  name      = "Panev"
  message   = "Panev's server"
  subnet_id = module.vpc_prod.public_subnet_ids[0] // the first sibnet
}

module "ec2servers_loop_count" {
  # source    = "../modules/ec2"
  source               = "git@gitlab.com:panev/terraform-lab.git//lab-24/modules/ec2"
  count     = length(module.vpc_prod.public_subnet_ids)
  name      = "Server-${count.index + 1}"
  message   = "Hello From server in Subnet ${module.vpc_prod.public_subnet_ids[count.index]} created by COUNT Loop"
  subnet_id = module.vpc_prod.public_subnet_ids[count.index] // takes different  IDs
}

module "servers_loop_foreach" {
  # source   = "../modules/ec2"
  source               = "git@gitlab.com:panev/terraform-lab.git//lab-24/modules/ec2"
  for_each = { for i, val in module.vpc_prod.public_subnet_ids : i => val }
  #   name       = "Server-${each.value + 1}"
  name       = "Server-${each.value}"
  message    = "Hello From server in Subnet ${each.value} created by FOR_EACH Loop"
  subnet_id  = each.value
  depends_on = [module.ec2servers_loop_count]
}
