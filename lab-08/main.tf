
#----------------------------------------------------------
#  Terraform 
#
# Outputs
#
#----------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "Server-Web" {
    ami                    =  "ami-07df274a488ca9195" // Amazin Linux 2
    instance_type          =  "t2.micro"
    vpc_security_group_ids =  [aws_security_group.web.id]
    tags                   =  {Name = "Server-Web"}

    depends_on = [
        aws_instance.Server-DB, 
        aws_instance.Server-App
        ]
}

resource "aws_instance" "Server-App" {
    ami                    =  "ami-07df274a488ca9195" // Amazin Linux 2
    instance_type          =  "t2.micro"
    vpc_security_group_ids =  [aws_security_group.web.id]
    tags                   =  {Name = "Server-App"} 

    depends_on = [aws_instance.Server-DB]
}

resource "aws_instance" "Server-DB" {
    ami                    =  "ami-07df274a488ca9195" // Amazin Linux 2
    instance_type          =  "t2.micro"
    vpc_security_group_ids =  [aws_security_group.web.id]
    tags                   =  {Name = "Server-DB"} 
}

resource "aws_security_group" "web" {
  name        = "WebServer-SG"
  description = "SG for my WebServer"

  dynamic "ingress" {
      for_each = ["80", "443", "10022", "21"]
      content {
        description = "Allow TCP ports"
        from_port   = ingress.value
        to_port     = ingress.value
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"] 
      }
  }

  dynamic "ingress" {
      for_each = ["9000", "9001", "9002", "9003"]
      content {
        description = "Allow UDP ports"
        from_port   = ingress.value
        to_port     = ingress.value
        protocol    = "udp"
        cidr_blocks = ["0.0.0.0/0"] 
      }
  }

    ingress {
    description = "radoslav.panev"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["78.83.166.190/32"]
  }

  egress {
    description = "Allow ALL port"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }

  tags = {
    Name  = "SG Build by Terraform"
    Owner = "Radoslav Panev"
  }

}
############## output ################

output "SecurityGroup_ID" {
  description = "SecurityGroup ID form my server"
  value = aws_security_group.web.id
}

# output "SecurityGroup_ID_all_details" {
#   description = "All details of my security group for my servers"
#   value = aws_security_group.web
# }

output "web_privete_ip" {
  description = "Print my privete IP"
  value = aws_instance.Server-Web.private_ip
}

output "app_privete_ip" {
  description = "Print my privete IP"
  value = aws_instance.Server-App.private_ip
}

output "db_privete_ips" {
  description = "Print my privete IP"
  value = aws_instance.Server-DB.private_ip
}

output "instance_ids" {
  value = [
    aws_instance.Server-Web.id,
    aws_instance.Server-App.id,
    aws_instance.Server-DB.id
  ]

}
output "ec2s_public_ips" {
  value = [
    aws_instance.Server-Web.public_ip,
    aws_instance.Server-App.public_ip,
    aws_instance.Server-DB.public_ip
  ]
}
