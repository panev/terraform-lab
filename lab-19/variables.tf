variable "env" {
  default = "test"
}

variable "server_size" {
  default = {
    prod       = "t2.large"
    staging    = "t2.small"
    dev        = "t2.medium"
    my_default = "t2.micro"
  }
}

variable "ami_id_per_region" {
  description = "My Custom AMI id per Region"
  default = {
    "eu-central-1" = "ami-05cafdf7c9f772ad2"
    "eu-west-1"    = "ami-01efa4023f0f3a042"
    "eu-west-2"    = "ami-0fdbd8587b1cf431e"
    "eu-west-3"    = "ami-031eb8d942193d84f"
  }
}

variable "allow_port" {
  default = {
    prod = ["80", "443"]
    rest = ["80", "443", "8080", "22"]
  }
}
