#----------------------------------------------------------
#  Terraform
#
# Build WebServer during Bootstrap
#
#----------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "web" {
  ami                    = "ami-07df274a488ca9195" // Amazin Linux 2
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.web.id]
  key_name               = "panev-aws"
  #    user_data         = "yum install http" // 1 ред
  user_data              = <<EOF
#!/bin/bash
yum -y update
yum install httpd -y
MYIP=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
echo "<h2>WebServer with PrivateIP: $MYIP</h2></br> Built by Terrafomr" > /var/www/html/index.html
service httpd start
chkconfig httpd on
EOF

  tags = {
    Name  = "Webserver Build by Terraform"
    Owner = "Radoslav Panev"
  }
}

resource "aws_security_group" "web" {
  name        = "WebServer-SG"
  description = "SG for my WebServer"

  ingress {
    description = "radoslav.panev"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["78.83.166.190/32"]
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow ALL port"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }

  tags = {
    Name  = "SG Build by Terraform"
    Owner = "Radoslav Panev"
  }
}