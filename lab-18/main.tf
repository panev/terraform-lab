
#----------------------------------------------------------
#  Terraform
#
# Execute Commands on Remote Server
#
#----------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "myserver" {
  ami                    = "ami-05cafdf7c9f772ad2"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.web.id]
  key_name               = "terraform"
  tags = {
    "Name"    = "Terraform-Test"
    "Project" = "Terraform-Lab"
    "Owner"   = "Radoslav Panev"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir /home/ec2-user/terraform",
      "cd /home/ec2-user/terraform",
      "touch terraform.txt",
      "echo Terraform was here on $(date) > terraform.txt"
    ]
    connection {
      type        = "ssh"
      user        = "ec2-user"
      host        = aws_instance.myserver.public_ip
      private_key = file("terraform.pem")
      timeout     = "3m"
    }
  }
}
resource "aws_security_group" "web" {
  name = "Terraform-Test"
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    description = "Allow ALL ports"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "Name"    = "Terraform-Test"
    "Project" = "Terraform-Lab"
    "Owner"   = "Radoslav Panev"
  }
}
