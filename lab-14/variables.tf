variable "aws_region" {
  description = "Region where you want to provision this EC2 instance"
  type        =  string // number , bool
  default     = "eu-central-1"
}

variable "port_list" {
  description = "List of Poret to open for our EC2 instance"
  type        = list(any)
  default     = [ "22", "80", "443" ]  
}

variable "instance_size" {
  description = "EC2 instance size to provision"
  default     = "t2.micro"
}

variable "tags" {
  description           = "Tags to apply to resources"
  type                  = map(any)
  default               = {
        Owner           = "Radoslav Panev"
        Environment     = "Demo"
        Project         = "Demo"
  }
}
variable "key_pair" {
  description = "SSH key pair bame ot ingest into EC2"  
  type          = string
  default      = "aws-panev"
  sensitive     = true
}

# variable "password" {
#   description = "Please Enter Password lenght of 10 characters!"
#   type        = string
#   sensitive   = true
#   validation {
#     condition     = length(var.password) == 10
#     error_message = "Your Password must be 10 characted exactly!!!"
#   }
# }

