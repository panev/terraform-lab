
#------------------------------------------------------------------
#  Terraform 
#
# Generate, Store and Retrieve Password Using SSM Parameter Store
#
#------------------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}

resource "aws_db_instance" "prod" {
    identifier             = "prod-mysql-rds"
    allocated_storage      = 20
    storage_type           = "gp2"
    engine                 = "mysql"
    engine_version         = "5.7"
    instance_class         = "db.t2.micro"
    # parameter_group_name   = "default.mysql.5.7"
    skip_final_snapshot    = true
    apply_immediately      = true
    username               = "administrator"
 #   password               = "0aqtj2If8bdwVdhAgOGv"
    password               = data.aws_ssm_parameter.rds_password.value
}


# Generate password
resource "random_password" "main" {
  length           = 20
  min_lower        = 5
  min_numeric      = 5
  min_upper        = 5
  min_special      = 5
  special          = true  # By default !@#$%&*()-_=+[]{}<>:?
  override_special = "#!()_-"
}

// Store password
resource "aws_ssm_parameter" "rds_password" {
  name         = "/prod/prod-mysql-rds/password"
  description  = "Mastar password for RDS Databases"
  type         = "SecureString"
  value        = random_password.main.result
}

# Retrive password

data "aws_ssm_parameter" "rds_password" {
  name         = "/prod/prod-mysql-rds/password"  // prod is env 
  depends_on = [
    aws_ssm_parameter.rds_password
  ] 
}


output "rds_endpoint" {
  value = aws_db_instance.prod.endpoint
}

output "rds_address" {
  value = aws_db_instance.prod.address
}

output "rds_port" {
  value = aws_db_instance.prod.port
}

output "rds_username" {
  value = aws_db_instance.prod.username
}

output "db_password" {
  value        =  data.aws_ssm_parameter.rds_password.value
  sensitive    = true
}

