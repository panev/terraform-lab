#-------------------------------------------------------------------------------
#  Terraform
#
# terraform import
# Create manual ec2 instan and run after that :
# teraform inti
# terraform import aws_instance.myweb i-0e91e8c086a723469
# terraform import aws_s3_bucket.mybucket my-s3-bucket-v3
# terraform import aws_security_group.ec2 sg-01c083d9fd4370828
#
# terraform state show aws_instance.myweb
# terraform state list
#-------------------------------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "myweb" {
#   ami           = "ami-04c921614424b07cd"
#   instance_type = "t2.micro"
#   vpc_security_group_ids = [aws_security_group.ec2.id]
#   tags = {
#     "Name" = "TEST"
#   }
}

resource "aws_s3_bucket" "mybucket" {
#   acl           = null
#   force_destroy = false
#   tags          = {}
 }

resource "aws_security_group" "ec2" {
#   description = "launch-wizard-1 created 2022-02-03T21:08:58.215+02:00"
}
