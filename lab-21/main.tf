
#----------------------------------------------------------
#  Terraform
#
# Loop Construct: for_each = toset([])
#                 for_each = {}
#
#----------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}

resource "aws_iam_user" "user" {
  for_each = toset(var.aws_users)
  name     = each.value
}

resource "aws_instance" "my_server" {
  for_each      = toset(["Dev", "Staging", "Prod"])
  ami           = "ami-05cafdf7c9f772ad2"
  instance_type = "t2.micro"
  tags = {
    Name = "Server-${each.value}"
  }
}


resource "aws_instance" "server" {
  for_each      = var.server_settings
  ami           = each.value["ami"]
  instance_type = each.value["instance_type"]

  root_block_device {
    volume_size = each.value["root_disksize"]
    encrypted   = each.value["encrypted"]
  }

  volume_tags = {
    "Name" = "Disk-${each.key}"
  }

  tags = {
    Name      = "Server-${each.key}"
    Terraform = true
  }
}

resource "aws_instance" "bastion_server" {
  for_each = var.create_bastion == "YES" ? toset(["bastion"]) : []
  ami           = "ami-05cafdf7c9f772ad2"
  instance_type = "t2.micro"
  tags = {
    Name = "Bastion-Servers"
  }  
}