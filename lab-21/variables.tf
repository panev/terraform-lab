variable "aws_users" {
  description = "List of IAM User to create"
  default = [
    "aws@gmail.bg",
    #   "info@gmail.bg",
    #   "test@gmail.bg",
    "vps@gmail.bg"
  ]
}

variable "server_settings" {
  type = map(any)
  default = {
    web = {
      ami           = "ami-05cafdf7c9f772ad2"
      instance_type = "t2.micro"
      root_disksize = 20
      encrypted     = true
    }
    app = {
      ami           = "ami-05cafdf7c9f772ad2"
      instance_type = "t2.micro"
      root_disksize = 10
      encrypted     = false
    }
  }

}

variable "create_bastion" {
  description = "Provision Bastion Servers YES/NO"
  default     = "NO"
}
