variable "environment" {
  default = "DEV"
}

variable "project_name" {
  default = "SKY"
}

variable "owner" {
  default = "Radoslav Panev"
}

variable "tags" {
  default = {
    budged_code = 11223344
    Manager     = "Elon Mask"
    Planet      = "Mars"
  }
}