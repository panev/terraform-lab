#-------------------------------------------------------------------------------
#  Terraform
#
# Terraform Logs (Debugging)
# Log Levels: TRACE, DEBUG, INFO, WARN, ERROR
# Enable Logs:
#   export TF_LOG=ERROR
#   export TF_LOG_PATH=terraform.logs
#
# Disable Logs:
#   unset TF_LOG
#   unset TF_LOG_PATH
#
#-------------------------------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "web" {
  ami           = "ami-04c921614424b07cd"
  instance_type = "t2.micro"
}
