variable "aws_region" {
  description = "Region where you want to provision this EC2 instance"
  type        =  string // number , bool
}

variable "port_list" {
  description = "List of Poret to open for our EC2 instance"
  type        = list(any)

}

variable "instance_size" {
  description = "EC2 instance size to provision"

}

variable "tags" {
  description           = "Tags to apply to resources"
  type                  = map(any)

}
variable "key_pair" {
  description = "SSH key pair bame ot ingest into EC2"  
  type          = string
  sensitive     = true
}


