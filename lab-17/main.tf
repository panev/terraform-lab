
#----------------------------------------------------------
#  Terraform
#
# Execute Commands on Local Terraform Server
#
#----------------------------------------------------------
provider "aws" {
  region = "eu-central-1"
}

resource "null_resource" "command1" {
  provisioner "local-exec" {
    command = "echo Terraform START: $(date) >> log.txt"
  }
}


resource "null_resource" "command2" {
  provisioner "local-exec" {
    command = "ping -c 5 www.google.com"
  }
}

resource "null_resource" "command3" {
  provisioner "local-exec" {
    interpreter = ["python3", "-c"]
    command     = "print('Hello Terraform!!!')"
  }
}

resource "null_resource" "command4" {
  provisioner "local-exec" {
    command = "echo $NAME1 $NAME2 $NAME3  >> log.txt"
    environment = {
      NAME1 = "Radoslav"
      NAME2 = "Panev"
      NAME3 = "Terraform"
    }
  }
}

resource "aws_instance" "myserver" {
  ami           = "ami-05f7491af5eef733a"
  instance_type = "t2.micro"
  key_name      = "aws-panev"
  tags = {
    Name = "Terraform-Test"
  }
  provisioner "local-exec" {
    command = "echo The Privete IP is : ${aws_instance.myserver.private_ip}, The Public IP is :  ${aws_instance.myserver.public_ip} >> log.txt"
  }
}

resource "null_resource" "command5" {
  provisioner "local-exec" {
    command = "echo Terraform FINISH: $(date) >> log.txt"
  }
  depends_on = [
    null_resource.command1,
    null_resource.command2,
    null_resource.command3,
    null_resource.command4,
    aws_instance.myserver
  ]
}
