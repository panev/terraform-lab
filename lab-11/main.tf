
#------------------------------------------------------------------
#  Terraform 
#
# Fetch and use information of:
# Current Region, List of Availability Zones, Account ID, VPCs
#
#------------------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}

data "aws_region" "current" {} 
data "aws_caller_identity" "current" {}
data "aws_availability_zones" "available" {
    state = "available"
}
data "aws_vpcs" "vpcs" {}

data "aws_vpc" "Demo" {
    tags = {
      "Name" = "panevVPC-Demo"
    }
}

resource "aws_subnet" "demo1" {
  vpc_id = data.aws_vpc.Demo.id
  availability_zone = data.aws_availability_zones.available.names[0]
  cidr_block = "172.16.3.0/24"

  tags = {
    "Name" = "Demo1"
    "Info" = "AZ: ${data.aws_availability_zones.available.names[0]} in Region: ${data.aws_region.current.description}"
    "Project" = "Demo"
  }
}

resource "aws_subnet" "demo2" {
  vpc_id = data.aws_vpc.Demo.id
  availability_zone = data.aws_availability_zones.available.names[2]
  cidr_block = "172.16.5.0/24"

  tags = {
    "Name" = "Demo2"
    "Info" = "AZ: ${data.aws_availability_zones.available.names[2]} in Region: ${data.aws_region.current.description}"
    "Project" = "Demo"
  }
}

######################################

// aws_region
output "region_name" {
  value = data.aws_region.current.name                 // The name of the selected region.
}

output "region_description" {
  value = data.aws_region.current.description         // The region's description in this format: "Location (Region name)".
}

output "region_endpoint" {
  value = data.aws_region.current.endpoint           // The EC2 endpoint for the selected region.
}


// aws_caller_identity
output "account_id" {
  value = data.aws_caller_identity.current.account_id   // AWS Account ID number of the account that owns or contains the calling entity.
}

output "caller_arn" {
  value = data.aws_caller_identity.current.arn         // ARN associated with the calling entity.
}

output "caller_id" {
  value = data.aws_caller_identity.current.id          // Account ID number of the account that owns or contains the calling entity.
}

output "caller_user" {
  value = data.aws_caller_identity.current.user_id      // Unique identifier of the calling entity.
}

// aws_availability_zones

output "group_names" {
  value = data.aws_availability_zones.available.group_names
}

output "names" {
  value = data.aws_availability_zones.available.names
}

output "state" {
  value = data.aws_availability_zones.available.state
}

output "zone_ids" {
  value = data.aws_availability_zones.available.zone_ids
}

// aws_vpcs
output "all_vpc_ids" {
  value = data.aws_vpcs.vpcs.ids
}
