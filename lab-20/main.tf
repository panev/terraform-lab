#----------------------------------------------------------
#  Terraform 
#
# Loop Construct: count = x
#
#----------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "servers" {
  count         = 4
  ami           = "ami-05cafdf7c9f772ad2"
  instance_type = "t2.micro"
  tags = {
    Name = "Server Number ${count.index + 1}"
    # if we remove "+ 1", we will have server names starting with 0
    Owner     = "Radoslav Panev"
    Terrafrom = "true"
  }
}

# resource "aws_instance" "server2" {
#   ami = "ami-05cafdf7c9f772ad2"
#   instance_type = "t2.micro"
#   tags = {
#       Name = "Server Number 2"
#   }
# }

# resource "aws_instance" "server3" {
#   ami = "ami-05cafdf7c9f772ad2"
#   instance_type = "t2.micro"
#   tags = { 
#       Name = "Server Number 3"
#   }
# }

# it is not recommended to use this method for creating users !!! problem deleting 1 user
resource "aws_iam_user" "user" {
  count = length(var.aws_users)
  name  = element(var.aws_users, count.index)
}

resource "aws_instance" "bastion_server" {
  count         = var.create_bastion == "YES" ? 1 : 0
  ami           = "ami-05cafdf7c9f772ad2"
  instance_type = "t2.micro"
  tags = {
    Name      = "Bastion Server"
    Owner     = "Radoslav Panev"
    Terrafrom = "true"
  }
}
