variable "aws_users" {
  description = "List of IAM User to create"
  default = [
      "radoslav.panev@gmail.com",
      "info@vbyte.org"
  ]
}

variable "create_bastion" {
  description = "Provision Bastion Server YES/NO"
  default     = "NO"
}