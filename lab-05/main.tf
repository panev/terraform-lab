#----------------------------------------------------------
#  Terraform
#
# Use Dynamic Blocks
#
#----------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}


resource "aws_security_group" "web" {
  name        = "WebServer-SG"
  description = "SG for my WebServer"



  dynamic "ingress" {
      for_each = ["80", "443", "10022", "21"]
      content {
        description = "Allow TCP ports"
        from_port   = ingress.value
        to_port     = ingress.value
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"] 
      }
  }

  dynamic "ingress" {
      for_each = ["9000", "9001", "9002", "9003"]
      content {
        description = "Allow UDP ports"
        from_port   = ingress.value
        to_port     = ingress.value
        protocol    = "udp"
        cidr_blocks = ["0.0.0.0/0"] 
      }
  }

    ingress {
    description = "radoslav.panev"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["78.83.166.190/32"]
  }

  egress {
    description = "Allow ALL port"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }

  tags = {
    Name  = "SG Build by Terraform"
    Owner = "Radoslav Panev"
  }
}