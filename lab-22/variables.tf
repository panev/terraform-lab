variable "aws_users" {
  description = "List of IAM Users to create"
  default = [
    "aws@gmail.bg",
    "info@gmail.bg",
    "test@gmail.bg",
    "vps@gmail.bg",
    "john",
    "robert",
    "paul",
    "victor",
    "dany"
  ]
}
