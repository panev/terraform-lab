#----------------------------------------------------------
#  Terraform
#
# Loop Construct: for x in xyz:
#
#----------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}

resource "aws_iam_user" "user" {
  for_each = toset(var.aws_users)
  name     = each.value
}

resource "aws_instance" "my_server" {
  count         = 4
  ami           = "ami-05cafdf7c9f772ad2"
  instance_type = "t2.micro"
  tags = {
    Name  = "Server-${count.index + 1}"
  }
}