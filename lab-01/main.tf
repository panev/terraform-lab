#----------------------------------------------------------
#  Terraform
#
# Build EC2 Instances
#
#----------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "my_ubuntu" {
  ami           = "ami-05f7491af5eef733a" # This is Comments
  instance_type = "t2.micro"              // This is also Comments
  key_name      = "panev-aws"

  tags = {
    Name    = "My-UbuntuLinux-Server"
    Owner   = "Radoslav Panev"
    project = "Phoenix"
    Version = "1.0.0"
  }
}

resource "aws_instance" "my_amazon" {
  ami           = "ami-07df274a488ca9195"  // This is Comments
  instance_type = "t2.small"               # This is also Comments

  tags = {
    Name  = "My-AmazonLinux-Server"
    Owner = "Radoslav Panev"
  }
}