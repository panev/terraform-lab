#------------------------------------------------------------------
#  Terraform
#
# Fetch and use information of:
#  Latest AMI id of Ubuntu 20.04
#  Latest AMI id of Amazon Linux2
#  Latest AMI id of Windows Server 2019
#
#------------------------------------------------------------------

provider "aws" {
     region = "eu-central-1"
#     region = "us-east-1"
}

# resource "aws_instance" "myserver" {
#   ami           = data.aws_ami.amazonlinux2.id
#   instance_type = "t2.micro"
# }

data "aws_ami" "latest_ubuntu20" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

data "aws_ami" "latest_amazonlinux" {
  owners      = ["137112412989"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}

data "aws_ami" "latest_windowserver2019" {
  owners      = ["801119661308"]
  most_recent = true
  filter {
    name   = "name"
    values = ["Windows_Server-2019-English-Full-Base-*"]
  }
}


##############################################

output "ubuntu" {
  value = data.aws_ami.latest_ubuntu20.id
}

output "AL2" {
  value = data.aws_ami.latest_amazonlinux.id
}

output "windows2019" {
  value = data.aws_ami.latest_windowserver2019.id
}
