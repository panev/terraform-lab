#!/bin/bash
#When a change is made to the script, the ec2 rebuilds (# aws_instance.web must be replaced) befor add EIP
yum -y update
yum install httpd -y
MYIP=`curl http://169.254.169.254/latest/meta-data/local-ipv4`

cat <<EOF > /var/www/html/index.html
<html>
<h2>Built by Power of <font color="red">Terrafomr</font></h2><br>

<!-- WebServer with static IP <br>-->
<br>
PrivateIP: $MYIP
<p>
<font color="blue">Version 4.0</font>
</html>
EOF

service httpd start
chkconfig httpd on