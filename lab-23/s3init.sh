#!/bin/bash

REGION="eu-central-1"
BUCKET_NAME="terraform-s3-vbyte-bucket"

# if [ ! $(aws s3 ls s3://${BUCKET_NAME} 2>/dev/null) ]; then
# 	echo "Bucket ${BUCKET_NAME} does not exist. Creating..."
# 	aws s3api create-bucket \
# 		--region ${REGION} --bucket ${BUCKET_NAME}
# 	aws s3api put-bucket-versioning \
# 		--region ${REGION} --bucket ${BUCKET_NAME} \
# 		--versioning-configuration Status=Enabled
# 	aws s3api put-public-access-block \
# 		--region ${REGION} --bucket ${BUCKET_NAME} \
# 		--public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"
# else
# 	echo "Bucket already exists"
# 	terraform init || echo "terraform missing or failing."
# fi


if [ ! $(aws s3 ls s3://${BUCKET_NAME} 2>/dev/null) ]; then
	echo "Bucket ${BUCKET_NAME} does not exist. Creating..."
	aws s3api create-bucket \
		--bucket ${BUCKET_NAME} --create-bucket-configuration LocationConstraint=${REGION}
	aws s3api put-bucket-versioning \
		--region ${REGION} --bucket ${BUCKET_NAME} \
		--versioning-configuration Status=Enabled
	aws s3api put-public-access-block \
		--region ${REGION} --bucket ${BUCKET_NAME} \
		--public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"
else
	echo "Bucket already exists"
	terraform init || echo "terraform missing or failing."
fi